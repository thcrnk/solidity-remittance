var Remittance = artifacts.require("./Remittance.sol");

contract('Remittance', function(accounts) {
  var contract;
  
  var owner = accounts[0];
  var receiver = accounts[1];
  var exchange = accounts[2];

  beforeEach(async () => {
    contract = await Remittance.new();
  });

  describe('Kill the contract', () => {
    it('should kill the contract', async () => {
      await contract.killContract();
      var contractOwner = await contract.owner();
      assert.equal(contractOwner, "0x", 'Contract is not destroyed.');
    });

    it('should throw exception for killing the contract as different of owner', async () => {
      await contract.killContract();
      await expectThrow(contract.killContract({from: receiver}));
    });
  });
});
