pragma solidity ^0.4.6;

contract Remittance {
    
    address public owner;
    address public sender;
    uint    public deadlineLimit = 864000; // 10 days
    uint    public ownerFee = 10000;
    uint    public ownerBalance;
    
    struct ReceiverStruct {
        address receiver;
        uint amount;
        uint deadline;
    }
    
    mapping (bytes32 => ReceiverStruct) public accountBalances;
    
    modifier onlyOwner() {
        require(msg.sender == owner);        
        _;
    }

    function Remittance() public {
        owner = msg.sender;
    }
    
    function sendEther(address receiver, address exchangeAddress, bytes32 passHash, uint deadline) public payable {
        require(msg.value > 0);
        require(msg.value > ownerFee);
        require(msg.sender != receiver);
        require(msg.sender != exchangeAddress);
        require(receiver != exchangeAddress);
        require(receiver != address(0));
        require(exchangeAddress != address(0));
        require(passHash != 0);
        require(deadline <= now + deadlineLimit);
        require(deadline >= now);
        
        bytes32 finalHash = keccak256(passHash, receiver, exchangeAddress);

        // This passwords and receivers combination could not be used more than once
        require(accountBalances[finalHash].receiver == 0);

        accountBalances[finalHash] = ReceiverStruct({
                receiver: receiver,
                amount: accountBalances[finalHash].amount + msg.value,
                deadline: deadline
        });
    }

    // The exchangeAddress should call withdraw
    function withdraw (address receiver, bytes32 passHash1, bytes32 passHash2) public payable {
        require(receiver != address(0));
        
        // msg.sender is our exchangeAddress
        bytes32 passHash = keccak256(passHash1, passHash2);
        bytes32 finalHash = keccak256(passHash, receiver, msg.sender);
        ReceiverStruct storage balanceStruct = accountBalances[finalHash];
        require(balanceStruct.deadline <= now);
        
        ownerBalance += ownerFee;
        uint amountToSend = balanceStruct.amount - ownerFee;
        delete accountBalances[finalHash];
        msg.sender.transfer(amountToSend);
    }
    
    function ownerWithdraw(uint amount) public payable onlyOwner {
        require(ownerBalance >= amount);
        
        ownerBalance -= amount;
        msg.sender.transfer(amount);
    }
    
    function killContract() public onlyOwner returns(bool success) {
        require(msg.sender == owner);
        selfdestruct(owner);
        
        return true;
    }
}